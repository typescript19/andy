"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logout = exports.login = exports.index = void 0;
const index = (req, res) => {
    var _a;
    if (((_a = req.cookies) === null || _a === void 0 ? void 0 : _a.islogin) === 'yes') {
        res.send('is login');
    }
    else {
        res.send('not login');
    }
};
exports.index = index;
const login = (req, res) => {
    if (req.body.account === 'abc' && req.body.password === '123') {
        res.cookie('islogin', 'yes');
        res.json({ result: 'ok' });
        return;
    }
    res.json({ result: 'fail' });
};
exports.login = login;
const logout = (req, res) => {
    res.clearCookie('islogin');
    res.send('logout');
};
exports.logout = logout;
//# sourceMappingURL=auth.js.map
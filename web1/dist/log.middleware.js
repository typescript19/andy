"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logMiddleware = void 0;
const logMiddleware = (req, res, next) => {
    console.log({
        header: req.headers,
        param: req.params,
        body: req.body,
        querystring: req.query,
    });
    next();
};
exports.logMiddleware = logMiddleware;
//# sourceMappingURL=log.middleware.js.map
import { default as express } from 'express';
import { default as dotenv } from 'dotenv';
import { default as morgan } from 'morgan';
import { default as bodyParser } from 'body-parser';
import { default as cookieParser } from 'cookie-parser';
import { useExpressServer, Controller, Get } from 'routing-controllers';
import 'reflect-metadata';
import { UserController } from './UserController';
import * as auth from './auth';
//以上皆為引入套件或是檔案

const app = express();
app.use(morgan('dev'));
app.use(express.json());
dotenv.config();
app.use(cookieParser()); //使用cookie-parser
app.use(bodyParser.json()); //使用body-parser
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.post('/', (request, response) => {
  response.send(request.body);
});

//routing-controllers 使用 UserController 檔案內的 controllers 去做判斷
//useExpressServer單獨創建和配置 express 應用程序
useExpressServer(app, {
  controllers: [UserController],
});

//express使用auth內部建立的函式
// app.get('/', auth.index);
// app.post('/login', auth.login);
// app.get('/logout', auth.logout);

app.listen(process.env.PORT);

